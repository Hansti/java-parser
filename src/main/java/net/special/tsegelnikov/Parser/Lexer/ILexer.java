package net.special.tsegelnikov.Parser.Lexer;

import net.special.tsegelnikov.Exeptions.IOExeptionFormatter;

import java.util.List;
import java.util.Queue;

/**
 * Returns lexemes
 */
public interface ILexer {
    /**
     * Return token.
     * @throws IOExeptionFormatter
     */
    String getLexeme() throws IOExeptionFormatter;

    /**
     * Checks if stream has next token.
     * @return true if stream has next token and false otherwise.
     * @throws IOExeptionFormatter
     */
    boolean hasNext() throws  IOExeptionFormatter;
}
