package net.special.tsegelnikov.Parser.Lexer;

import net.special.tsegelnikov.Exeptions.IOExeptionFormatter;
import net.special.tsegelnikov.IO.InputReaders.IInputReader;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Hanst on 20.05.2016.
 */
public class Lexer implements ILexer {
    private static final Logger log = Logger.getLogger(Lexer.class);
    String lexeme;
    IInputReader iInputReader;
    StringBuilder buffer;
    char aChar;

    public Lexer(IInputReader iInputReader){
        this.iInputReader = iInputReader;
        buffer = new StringBuilder();
    }

    public boolean hasNext() throws IOExeptionFormatter {
        return iInputReader.hasNext();
    }

    public String getLexeme() throws IOExeptionFormatter {
        lexeme = "";

        if(buffer.length() != 0){
            lexeme = buffer.toString();
            buffer.delete(0, buffer.length());
            return lexeme;
        }

        while(iInputReader.hasNext()){
            aChar = iInputReader.readInput();

            if ((Character.isLetterOrDigit(aChar)) || (aChar == '+') || (aChar == '-') || (aChar == '=')){
                buffer.append(aChar);
            }else if (Character.isSpaceChar(aChar) || aChar == '\n' || aChar == '\t'){
                if(buffer.length() != 0) {
                    lexeme = buffer.toString();
                    buffer.delete(0, buffer.length());
                    break;
                }
            }else {
                if(buffer.length() != 0){
                    lexeme = buffer.toString();
                    buffer.delete(0, buffer.length());
                    buffer.append(aChar);

                    break;
                }

                buffer.append(aChar);
                lexeme = buffer.toString();
                buffer.delete(0, buffer.length());

                break;
            }
        }

        return lexeme;
    }
}
