package net.special.tsegelnikov.Parser;

import net.special.tsegelnikov.Exeptions.IOExeptionFormatter;
import net.special.tsegelnikov.IO.InputReaders.IInputReader;
import net.special.tsegelnikov.IO.OutputWriters.IOutputWriter;

/**
 * Returns formatted code
 */
public interface IFormatter {

    /**
     * Formats code depending on current state of formatted code.
     * @param inputReader
     * @param outputWriter
     * @throws IOExeptionFormatter
     */
    public void format(IInputReader inputReader, IOutputWriter outputWriter) throws IOExeptionFormatter;
}
