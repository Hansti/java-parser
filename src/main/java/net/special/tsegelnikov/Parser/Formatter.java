package net.special.tsegelnikov.Parser;

import net.special.tsegelnikov.Exeptions.IOExeptionFormatter;
import net.special.tsegelnikov.IO.InputReaders.IInputReader;
import net.special.tsegelnikov.IO.OutputWriters.IOutputWriter;
import net.special.tsegelnikov.Parser.Handler.Handler;
import net.special.tsegelnikov.Parser.Handler.Handlers.DefaultTokenHandler;
import net.special.tsegelnikov.Parser.Handler.IHandler;
import net.special.tsegelnikov.Parser.Lexer.Lexer;
import net.special.tsegelnikov.Parser.State;
import org.apache.log4j.Logger;

import java.util.Queue;

public class Formatter implements IFormatter{
    private static final Logger log = Logger.getLogger(Formatter.class);
    State state;
    Handler handler;
    StringBuilder output;
    Lexer lexer;

    public Formatter(){
        output = new StringBuilder();
        state = new State();
        handler = new Handler();
    }

    public void format(IInputReader inputReader, IOutputWriter outputWriter) throws IOExeptionFormatter {

        lexer = new Lexer(inputReader);

        while (lexer.hasNext()){
            String token = lexer.getLexeme();
                IHandler handlerToken = handler.choose(token);
                handlerToken.format(token);
                output.append(handlerToken.getCode());
        }

        log.info("return formatted code");

        outputWriter.writeOutput(output);
    }

}
