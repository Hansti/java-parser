package net.special.tsegelnikov.Parser.Handler.Handlers;

import net.special.tsegelnikov.Parser.Handler.IHandler;
import net.special.tsegelnikov.Parser.State;

/**
 * Created by Hanst on 20.05.2016.
 */
public class CloseCurlyBracketHandler implements IHandler{
    private StringBuilder output;
    private State state;

    public CloseCurlyBracketHandler(State state){
        this.state = state;
    }

    public void format(String token) {
        output = new StringBuilder();

        output.append('\n');
        state.setDeep(state.getDeep() - 1);

        for(int i = 0; i < state.getDeep(); i++){
            output.append("    ");
        }

        output.append('}');
        state.setEndOfLine(true);
    }


    public StringBuilder getCode() {
        return output;
    }


}
