package net.special.tsegelnikov.Parser.Handler.Handlers;

import net.special.tsegelnikov.Parser.Handler.IHandler;
import net.special.tsegelnikov.Parser.State;

/**
 * Created by Hanst on 20.05.2016.
 */
public class DefaultTokenHandler implements IHandler{
    private StringBuilder output;
    private State state;

    public DefaultTokenHandler(State state){
        this.state = state;
    }


    public void format(String token) {
        output = new StringBuilder();

        output.append(' ');

        if (state.getEndOfLine()){
            output.append('\n');

            for(int i = 0; i < state.getDeep(); i++){
                output.append("    ");
            }

            state.setEndOfLine(false);
        }

        output.append(token);
    }

    public StringBuilder getCode() {
        return output;
    }
}
