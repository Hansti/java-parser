package net.special.tsegelnikov.Parser.Handler;

import net.special.tsegelnikov.Parser.State;

public interface IHandler {
    public void format(String token);
    public StringBuilder getCode();
}
