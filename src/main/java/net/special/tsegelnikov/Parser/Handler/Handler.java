package net.special.tsegelnikov.Parser.Handler;

import net.special.tsegelnikov.Parser.Handler.Handlers.CloseCurlyBracketHandler;
import net.special.tsegelnikov.Parser.Handler.Handlers.DefaultTokenHandler;
import net.special.tsegelnikov.Parser.Handler.Handlers.OpenCurlyBracketHandler;
import net.special.tsegelnikov.Parser.Handler.Handlers.SemicolonHandler;
import net.special.tsegelnikov.Parser.State;
import net.special.tsegelnikov.Settings.Settings;
import org.apache.log4j.Logger;

import java.util.HashMap;

/**
 * Picks appropriate formatter class for symbol from input.
 */
public class Handler{
    private static final Logger log = Logger.getLogger(Handler.class);
    State state = new State();
    private HashMap<String, IHandler> handlers;
    {
        handlers = new HashMap<>();
        handlers.put("{", new OpenCurlyBracketHandler(state));
        handlers.put("}", new CloseCurlyBracketHandler(state));
        handlers.put(";", new SemicolonHandler(state));
    }

    public Handler() {
    }

    public IHandler choose(String token) {
        return handlers.getOrDefault(token, new DefaultTokenHandler(state));
    }
}
