package net.special.tsegelnikov.Parser.Handler.Handlers;

import net.special.tsegelnikov.Parser.Handler.IHandler;
import net.special.tsegelnikov.Parser.State;

/**
 * Created by Hanst on 20.05.2016.
 */
public class OpenCurlyBracketHandler implements IHandler{
    private StringBuilder output;
    private State state;

    public OpenCurlyBracketHandler(State state){
        this.state = state;
    }

    public void format(String token) {
        output = new StringBuilder();
        output.append('{');
        state.setDeep(state.getDeep() + 1);
        state.setEndOfLine(true);
    }

    public StringBuilder getCode() {
        return output;
    }

}
