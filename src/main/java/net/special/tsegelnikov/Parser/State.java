package net.special.tsegelnikov.Parser;

/**
 * Created by Hanst on 20.05.2016.
 */
public class State {
    private int deep;
    private boolean inFor;
    private boolean inComment;
    private boolean endOfLine;

    public State(){
        deep = 0;
        inFor = false;
        inComment = false;
        endOfLine = false;
    }

    public void setDeep(int deep) {
        this.deep = deep;
    }

    public int getDeep() {
        return deep;
    }

    public void setInComment(boolean inComment) {
        this.inComment = inComment;
    }

    public boolean getInComment(){
        return inComment;
    }

    public void setInFor(boolean inFor) {
        this.inFor = inFor;
    }

    public boolean getInFor(){
        return inFor;
    }

    public void setEndOfLine(boolean endOfLine){
        this.endOfLine = endOfLine;
    }

    public boolean getEndOfLine(){
        return endOfLine;
    }

}
