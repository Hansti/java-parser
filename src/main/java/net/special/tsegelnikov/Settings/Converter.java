package net.special.tsegelnikov.Settings;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

/**
 * Created by Hanst on 09.11.2015.
 */
public class Converter {
    private final static String baseFile = "settings.json";

    public Converter(){
    }

    public static void toJSON(Settings settings) throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File(baseFile), settings);
    }

    public static Settings toJavaObject() throws IOException{
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(new File(baseFile), Settings.class);
    }
}
