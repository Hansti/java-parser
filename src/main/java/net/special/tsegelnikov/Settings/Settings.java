package net.special.tsegelnikov.Settings;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.IOException;

/**
 * Created by Hanst on 08.11.2015.
 */

public class Settings {
    @JsonProperty("Counter Space")
    private int countSpace = 4;

    @JsonProperty("Directory")
    private String dir = "test.txt";

    private final static String settingFile = "setting.json";

    public Settings(){
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getDir() {
        return dir;
    }

    public void setCountSpace(int countSpace) {
        this.countSpace = countSpace;
    }

    public int getCountSpace() {
        return countSpace;
    }
}
