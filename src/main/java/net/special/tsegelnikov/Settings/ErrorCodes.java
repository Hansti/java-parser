package net.special.tsegelnikov.Settings;

/**
 * Created by Hanst on 09.11.2015.
 */
public enum ErrorCodes{
    FAIL_READ_FILE("Error reading file"),
    FAIL_WRITE_FILE("Error write file"),
    FAIL_PARSING("Error parsing");

    private final String errorString;

    ErrorCodes(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString(){
        return errorString;
    }
}