package net.special.tsegelnikov.IO.OutputWriters;

import net.special.tsegelnikov.Exeptions.IOExeptionFormatter;
import org.apache.log4j.Logger;

/**
 * Created by Hanst on 18.05.2016.
 */
public class StringOutputWriter implements IOutputWriter{
    private StringBuilder stringBuilder;

    public StringOutputWriter(StringBuilder stringBuilder){
        this.stringBuilder = stringBuilder;
    }

    public void writeOutput(StringBuilder input) throws IOExeptionFormatter {
        stringBuilder.append(input);
    }
}
