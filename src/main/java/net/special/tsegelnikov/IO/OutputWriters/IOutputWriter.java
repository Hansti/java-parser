package net.special.tsegelnikov.IO.OutputWriters;

import net.special.tsegelnikov.Exeptions.IOExeptionFormatter;

/**
 * Handles output stream of symbols.
 */
public interface IOutputWriter {

    /**
     * Writes symbol to stream.
     * @param input containing output.
     * @throws IOExeptionFormatter
     */

    public void writeOutput(StringBuilder input) throws IOExeptionFormatter;
}
