package net.special.tsegelnikov.IO.InputReaders;

import net.special.tsegelnikov.Exeptions.IOExeptionFormatter;

/**
 * Handles input stream.
 */
public interface IInputReader {

    /**
     * Checks if stream has next value.
     * @return true if stream has next value and false otherwise.
     * @throws IOExeptionFormatter
     */
    boolean hasNext() throws IOExeptionFormatter;

    /**
     * Reads next symbol from stream.
     * @return next char in stream.
     * @throws IOExeptionFormatter
     */
    char readInput() throws IOExeptionFormatter;

    /**
     * Closes stream.
     * @throws IOExeptionFormatter
     */
    void close() throws IOExeptionFormatter;
}
