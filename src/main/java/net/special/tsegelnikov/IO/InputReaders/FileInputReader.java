package net.special.tsegelnikov.IO.InputReaders;

import net.special.tsegelnikov.Exeptions.IOExeptionFormatter;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Hanst on 18.05.2016.
 */
public class FileInputReader implements IInputReader{
    private static final Logger log = Logger.getLogger(FileInputReader.class);
    private File file;
    private InputStream inputStream;

    public FileInputReader(File file) throws IOExeptionFormatter {
        this.file = file;
        try{
            inputStream = new FileUtils().openInputStream(file);
        }catch (IOException e){
            log.error("Reading the file has failed");
            throw new IOExeptionFormatter(e.getMessage(), e);
        }
    }

    public boolean hasNext() throws IOExeptionFormatter {
        try {
            return inputStream.available() != 0;
        }catch (IOException e){
            log.error("method hasNext() has failed");
            throw new IOExeptionFormatter(e.getMessage(), e);
        }
    }

    public char readInput() throws IOExeptionFormatter {
        try{
            return (char)inputStream.read();
        } catch (IOException e) {
            log.error("method readInput() has failed");
            throw new IOExeptionFormatter(e.getMessage(), e);
        }
    }

    public void close() throws IOExeptionFormatter {
        try {
            inputStream.close();
        } catch (IOException e) {
            log.error("close file has failed");
            throw new IOExeptionFormatter(e.getMessage(), e);
        }
    }
}
