package net.special.tsegelnikov.IO.InputReaders;

import net.special.tsegelnikov.Exeptions.IOExeptionFormatter;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Created by Hanst on 18.05.2016.
 */
public class StringInputReader implements IInputReader {
    private static final Logger log = Logger.getLogger(StringInputReader.class);
    private String string;
    private InputStream inputStream;

    public StringInputReader(String string, String charset) {
        this.string = string;
        inputStream = new IOUtils().toInputStream(string, Charset.forName(charset));
    }

    public boolean hasNext() throws IOExeptionFormatter {
        try{
            return inputStream.available() != 0;
        } catch (IOException e) {
            log.error("method hasNext() has failed");
            throw new IOExeptionFormatter(e.getMessage(), e);
        }
    }

    public char readInput() throws IOExeptionFormatter {
        try {
            return (char) inputStream.read();
        } catch (IOException e) {
            log.error("method readInput() has failed");
            throw new IOExeptionFormatter(e.getMessage(), e);
        }
    }

    public void close() throws IOExeptionFormatter {
        try {
            inputStream.close();
        } catch (IOException e) {
            log.error("close stream has failed");
            throw new IOExeptionFormatter(e.getMessage(), e);
        }
    }
}
