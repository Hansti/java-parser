package net.special.tsegelnikov.Exeptions;

import org.apache.log4j.Logger;

/**
 * Created by Hanst on 18.05.2016.
 */
public class IOExeptionFormatter extends Exception{
    private static final Logger log = Logger.getLogger(IOExeptionFormatter.class);

    /**
     * Constructs new exception with provided message.
     */
    public IOExeptionFormatter(String message){
        super(message);
        log.error(message);
    }

    /**
     * Constructs new excepton with provided message and cause.
     *
     **/
    public IOExeptionFormatter(String message, Throwable cause){
        super(message, cause);
        log.error(message);
    }
}
