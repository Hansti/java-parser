package net.special.tsegelnikov;

import net.special.tsegelnikov.Exeptions.IOExeptionFormatter;
import net.special.tsegelnikov.IO.InputReaders.StringInputReader;
import net.special.tsegelnikov.IO.OutputWriters.StringOutputWriter;
import net.special.tsegelnikov.Parser.Formatter;
import org.junit.Test;

/**
 * Created by Hanst on 20.05.2016.
 */
public class ParserTest {
    @Test
    public void parserTest(){
        String input= "while(1){a\n\n\n\n=\t temp1;\n\t\n      if(true){temp1 = 3;} temp1 += 1;}";
        StringBuilder output = new StringBuilder();

        Formatter formatter = new Formatter();

        try {
            formatter.format(new StringInputReader(input, "UTF-8"), new StringOutputWriter(output));
        } catch (IOExeptionFormatter ioExeptionFormatter) {
            ioExeptionFormatter.printStackTrace();
        }

        System.out.print(output.toString());
    }
}
