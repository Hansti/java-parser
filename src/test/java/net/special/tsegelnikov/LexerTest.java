package net.special.tsegelnikov;

import net.special.tsegelnikov.Exeptions.IOExeptionFormatter;
import net.special.tsegelnikov.IO.InputReaders.StringInputReader;
import net.special.tsegelnikov.Parser.Lexer.Lexer;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by Hanst on 20.05.2016.
 */
public class LexerTest {
    @Test
    public void testLexers(){
        String input= "while(1){a\n\n\n\n=\ttemp1;\n\t\n      if(true){temp1 = 3;} temp1 += 1;}";
        List<String> controlList = new LinkedList<>();
        List<String> tokens = new LinkedList<>();

        tokens.add("while");
        tokens.add("(");
        tokens.add("1");
        tokens.add(")");
        tokens.add("{");
        tokens.add("a");
        tokens.add("=");
        tokens.add("temp1");
        tokens.add(";");
        tokens.add("if");
        tokens.add("(");
        tokens.add("true");
        tokens.add(")");
        tokens.add("{");
        tokens.add("temp1");
        tokens.add("=");
        tokens.add("3");
        tokens.add(";");
        tokens.add("}");
        tokens.add("temp1");
        tokens.add("+=");
        tokens.add("1");
        tokens.add(";");
        tokens.add("}");

        Lexer lexer = new Lexer(new StringInputReader(input, "UTF-8"));
        try {
            while (lexer.hasNext()){
                controlList.add(lexer.getLexeme());
            }
        } catch (IOExeptionFormatter ioExeptionFormatter) {
            ioExeptionFormatter.printStackTrace();
        }

        assertTrue(controlList.equals(tokens));
    }
}
